import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayedQueueImpl {
    public static void main(String[] args) {
        BlockingQueue<DelayObject> queue = new DelayQueue<>();
        Thread t1= new Thread(() -> {
            queue.add(new DelayObject("This is first Object", 4000));
            queue.add(new DelayObject("This is second Object", 2000));
            queue.add(new DelayObject("This is third Object", 10000));
            queue.add(new DelayObject("This is fourth Object", 18000));

            while( !queue.isEmpty()) {
                try {
                    System.out.println(queue.take());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        t1.start();
    }
}
class DelayObject implements Delayed {
    @Override
    public String toString() {
        return "DelayObject{" +
                "data='" + data + '\'' +
                ", startTime=" + startTime +
                '}';
    }

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    private long startTime;

    public DelayObject(String data, long delayInMilliseconds) {
        this.data = data;
        this.startTime = System.currentTimeMillis() + delayInMilliseconds;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long diff = startTime - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed anotherDelayObject) {
        if (this.startTime < ((DelayObject)anotherDelayObject).getStartTime())
            return -1;
        if (this.startTime > ((DelayObject)anotherDelayObject).getStartTime())
            return 1;
        return 0;
    }
}
