import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class PriorityQueueImpl {

    public static void main(String[] args) {
        BlockingQueue<String> queue = new PriorityBlockingQueue<>();
        queue.add("D");
        queue.add("Z");
        queue.add("B");
        queue.add("H");
        queue.add("K");
        queue.add("I");

        traverseQueue(queue);

        BlockingQueue<Person> personQueue = new PriorityBlockingQueue<>();
        personQueue.add(new Person(33, "Harsh"));
        personQueue.add(new Person(3, "Aadira"));
        personQueue.add(new Person(29, "Anshul"));

        traverseQueue(personQueue);

    }

    private static void traverseQueue(BlockingQueue<?> queue) {
        while (!queue.isEmpty()) {
            try {
                System.out.println(queue.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
class Person implements Comparable<Person> {
    int age;
    String name;

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(Person anotherPerson) {
        return this.getName().compareTo(anotherPerson.getName());
    }
}
