import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockImpl {

    public static void main(String[] args) throws InterruptedException {

        Process process = new Process();
        Thread t1 = new Thread(() -> {
            for (int i = 1 ; i <= 10; i++) {
                process.produce(i);
            }
        }, "Thread A");

        Thread t2 = new Thread(() -> {
            for (int i = 1 ; i <= 10; i++) {
                process.consume();
            }
        }, "Thread B");
        t1.start();
        t2.start();
    }
}

class Process {

    ReentrantLock lock = new ReentrantLock();
    Condition condition = lock.newCondition();

    List<Integer> queue = new ArrayList<>();
    static final int MAX_LIMIT = 5;


    public void produce(int counter) {
        lock.lock();
        System.out.println("Producer Method Entered");
        if (queue.size() == MAX_LIMIT) {
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        queue.add(counter);
        System.out.println("Producing ..." + counter);
        condition.signal();
        lock.unlock();
    }

    public void consume()  {
        lock.lock();
        System.out.println("Consumer Method Entered");
        if (queue.isEmpty()) {
            try {
                condition.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        Integer remove = queue.remove(0);
        System.out.println("Consuming ..." + remove);
        condition.signal();
        lock.unlock();
    }
}
