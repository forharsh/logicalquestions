package assignment;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */
class Student {
    private String name;
    private String marks;

    public Student() {

    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getMarks() {
        return this.marks;
    }
}

class Solution {
    public static void main(String[] args) {
        if (pass()) {
            System.out.println("Pass");
        } else {
            System.out.println("Some Fail");
        }
    }

    public static Integer bestAvgGrade(String[][] scores) {
        List<Student> studentList = new ArrayList<>();
        for (int i = 0; i < scores.length; i++) {
            Student student = new Student();
            for (int j = 0; j < scores[i].length; j++) {
                if (j == 0) {
                    student.setName(scores[i][j]);
                } else {
                    student.setMarks(scores[i][j]);
                }
            }
            studentList.add(student);
        }
        Map<String, Double> resultMap = studentList.stream()
                .collect(Collectors.groupingBy(Student::getName, Collectors.averagingInt(value -> Integer.parseInt(value.getMarks()))));
        Optional<Map.Entry<String, Double>> max = resultMap.entrySet().stream().max(Map.Entry.comparingByValue());
        return max.get().getValue().intValue();
    }

    public static boolean pass() {
        String[][] s1 = {{"Rohan", "84"},
                {"Sachin", "102"},
                {"Ishan", "55"},
                {"Sachin", "18"}};

        return bestAvgGrade(s1) == 84;
    }
}

