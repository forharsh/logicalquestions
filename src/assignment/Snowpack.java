package assignment;

public class Snowpack {
    static int  maxWater(int arr[], int n)
    {

        // To store the maximum water
        // that can be stored
        int res = 0;

        // For every element of the array
        for (int i = 1; i < n-1; i++) {

            // Find the maximum element on its left
            int left = arr[i];
            for (int j=0; j<i; j++)
                left = Math.max(left, arr[j]);

            System.out.println(left);
            // Find the maximum element on its right
            int right = arr[i];
            for (int j=i+1; j<n; j++)
                right = Math.max(right, arr[j]);

            // Update the maximum water
            res = res + (Math.min(left, right) - arr[i]);
        }

        return res;
    }

    public static void main(String[] args) {
        int arr[] = {3,1,5,0};
        System.out.println(maxWater(arr, arr.length)); // 1
    }
}
