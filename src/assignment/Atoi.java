package assignment;

public class Atoi {
    // A simple atoi() function
    static int myAtoi(String str)
    {
        int res = 0; // Initialize result

        // Iterate through all characters of
        // input string and update result
        for (int i = 0; i < str.length(); ++i)
            res = res * 10 + str.charAt(i) - '0';

        // return result.
        return res;
    }

    // Driver code
    public static void main(String[] args)
    {
        String str = "89789fgdfghfhg";
        int val = myAtoi(str);
        System.out.println(val);
    }
}
