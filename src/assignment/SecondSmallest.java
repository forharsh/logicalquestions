package assignment;

import java.lang.reflect.Array;
import java.util.Arrays;

import static java.util.Arrays.sort;

public class SecondSmallest {
    /**
     * int secondSmallest(int[] x)
     */
    public static int secondSmallest(int[] x) {
        if (x.length == 1) {
            return x[0];
        } else if (x.length > 1) {
            sort(x);
            return x[1];
        }
        return 0;
    }

    public static boolean doTestsPass() {
        int[] a = {0};
        int[] b = {0, 1};

        boolean result = true;
        result &= secondSmallest(a) == 0;
        result &= secondSmallest(b) == 1;

        if (result) {
            System.out.println("Pass");
        } else {
            System.out.println("Fail");
        }
        return result;
    }

    public static void main(String args[]) {
        doTestsPass();
    }
}
