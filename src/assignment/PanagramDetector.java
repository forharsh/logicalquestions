package assignment;

public class PanagramDetector {
    public static void main(String[] args) {
        String str = "The quick brown fox jumps over the lazy dog";
        boolean chars[] = new boolean[26];

        char[] chars1 = str.toCharArray();

       for (char ch : chars1) {
           int index = 0;
          if ('A' <= ch && ch <= 'Z') {
              index = ch - 'A';
          } else if ('a' <= ch && ch <= 'z') {
              index = ch - 'a';
          }
          chars[index] = true;
        }
       boolean result = true;
        for (boolean ch : chars) {
           if (ch == false) {
               result = false;
               break;
           }
        }

        System.out.println(result);

    }
}
