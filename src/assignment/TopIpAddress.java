package assignment;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TopIpAddress {

    /**
     * Given a log file, return IP address(es) which accesses the site most often.
     */

    public static String findTopIpaddress(String[] lines) {
        String topIpAddress = "";
        Map<String, Long> collect = Stream.of(lines)
                .filter(Objects::nonNull)
                .map(line -> line.split(" - ")[0])
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Optional<Map.Entry<String, Long>> optionalMaxIpAddress = collect.entrySet().stream().max(Map.Entry.comparingByValue());
        if (optionalMaxIpAddress.isPresent()) {
            topIpAddress = optionalMaxIpAddress.get().getKey();
        }
        return topIpAddress;
    }

    public static void main(String[] args) {

        Map<String, String> map = new HashMap();
        map.put("h","v");
       // System.out.println(map.get);

        String lines[] = new String[] {
                "10.0.0.1 - log entry 1 11",
                "10.0.0.1 - log entry 2 213",
                "10.0.0.2 - log entry 133132" };
        String result = findTopIpaddress(lines);

        if (result.equals("10.0.0.1")) {
            System.out.println("Test passed");

        } else {
            System.out.println("Test failed");

        }

    }

}