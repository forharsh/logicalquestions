package assignment;

public class LongestPallindrome {

    public static void main(String[] args) {
        if (
                longestPalindrome("9912321456").equals("12321")
                        && longestPalindrome("12233213").equals("2332")
        ) {
            System.out.println("Tests passed");
        } else {
            System.out.println("Tests failed");
        }

    }

    public static String longestPalindrome(String str) {
        String res = "";
        for (int i = 0; i < str.length(); i++) {
            String even = aroundeven(str, i);
            String odd = aroundodd(str, i);

            String temp = odd.length() > even.length() ? odd : even;

            if (temp.length() > res.length()) {
                res = temp;
            }
        }

        return res;

    }

    public static String aroundeven(String str, int index) {
        int i = index;
        int j = index + 1;
        int len = 0;
        while (i >= 0 && j < str.length()) {
            char ch1 = str.charAt(i);
            char ch2 = str.charAt(j);

            if (ch1 != ch2) {
                break;

            }
            len++;
            i--;
            j++;
        }
        String res = str.substring(index - len + 1, index + len + 1);

        return res;
    }

    public static String aroundodd(String str, int index) {
        int i = 1;
        int maxlen = 0;
        int clen = 0;
        int len = str.length();
        while (i <= index && index - i > 0 && index + i < len) {
            char ch1 = str.charAt(index - i);
            char ch2 = str.charAt(index + i);

            if (ch1 == ch2) {
                clen++;
            }

            i++;
        }
        String res = str.substring(index - clen, index + clen + 1);

        return res;
    }
}
