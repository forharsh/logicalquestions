package assignment;

import java.util.HashMap;
import java.util.Map;

public class SumPair {
    public static void main(String[] args) {
        int [] arr = {-1,5,6,11,-1,3,4,5,7,11};
        System.out.println(sumPair(arr, 10));
    }

    public static String sumPair(int [] ar, int sum){

        Map<Integer, Integer> map = new HashMap<>();

        for(int a: ar){
            int count =0;
            if(map.containsKey(a)){
                count = map.get(a);
            }
            map.put(a,count+1);
        }

        StringBuilder sb = new StringBuilder();
        int  count =0;
        for(int a: ar){
            int diff = sum-a;
            if(map.containsKey(diff) && map.containsKey(a)){
                sb.append("["+a+","+diff+"],");
                count++;
                add_remove(map, a);
                add_remove(map, diff);
            }
        }
        return count+"  pairs with sum "+ sum +" >> "+sb.toString();
    }

    public static void add_remove(Map<Integer, Integer> map ,int  key){
        int keycount = map.get(key);
        if(keycount>1){
            map.put(key, keycount-1);
        }
        else{
            map.remove(key);
        }
    }
}
