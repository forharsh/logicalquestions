package assignment;

public class ThirdSmallest {
    public static void main(String[] args) {
        int a[] = { 12,11,15,5,9,100 };
        int smallest = Integer.MAX_VALUE;
        int secondsmallest = Integer.MAX_VALUE;
        int thirdsmallest = Integer.MAX_VALUE;
        for (int i = 0 ; i < a.length; i++) {
            if (smallest > a[i]) {
                thirdsmallest = secondsmallest;
                secondsmallest = smallest;
                smallest = a[i];
            } else if (secondsmallest > a[i]) {
                thirdsmallest = secondsmallest;
                secondsmallest = a[i];
            } else if (thirdsmallest > a[i]) {
                thirdsmallest = a[i];
            }
        }
        System.out.println(smallest);
        System.out.println(secondsmallest);
        System.out.println(thirdsmallest);
    }
}
