package assignment;

public class FibanociSerires {
    public static void main(String[] args) {

        System.out.println(fibRecursive(9));
        System.out.println(fibIterative(9));
    }

    public static int fibIterative(int n) {
        int f0 = 0;
        int f1 = 1;
        int temp = -1;
        for (int i = 0; i < n - 1; i++) {
            temp = f1 + f0;
            f0 = f1;
            f1 = temp;
        }
        return f1;
    }

    public static int fibRecursive(int n) {
        if (n < 2) {
            return n;
        }
        return fibRecursive(n - 1) + fibRecursive(n - 2);
    }
}
