package assignment;

import java.util.Arrays;

public class SmallestinRotatedArray {
    public static void main(String[] args) {
        // 2 3 4 5 6 7 8 9 10
        int arr[] = {6,7,8,9,10,2,3,4,5};
        int k =3;
        System.out.println(k+"th smallest "+nthSmallest(arr,k));

    }

    public static int nthSmallest(int [] arr, int k){
        int smallestIndex = smallest(arr);
        k = k<1 ? 0: k-1 ;
        int index = (k+smallestIndex)% arr.length;
        return arr[ index];

    }

    public static int smallest(int [] arr){
        int start =0;
        int end = arr.length-1;
        int mid = (start+end)/2;

        while(start<=end){
            mid = (start+end)/2;
            int prev = mid == 0 ? arr.length-1 : mid-1;

            if(arr[mid]< arr[prev]){
                return mid;
            }
            else{
                //go left
                if((arr[mid]>arr[start] && arr[mid] <arr[end])|| arr[start]>arr[mid]){
                    end = mid-1;
                }//go right
                else{
                    start = mid+1;
                }
            }
        }

        return mid;

    }
}
